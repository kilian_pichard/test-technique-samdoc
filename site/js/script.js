let returnD = document.getElementById("return");
let contentD = document.getElementById("content");
//Récupération de la racine
getFolderFiles('/');

function getFolderFiles(path) {
  //Réinitialisation de la page
  contentD.innerText = "";
  returnD.innerText = "";
  //requete pour recupérer les fichiers/dossiers
  var xhr = new XMLHttpRequest();
  xhr.open('GET', 'http://localhost:3000/api' + path, true);
  var New = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      let result = JSON.parse(xhr.response);
      //Filtres et Tri par Type de fichier (Dossier ou "document") et par ordre alphabétique
      let folder = result.filter(a => a.dir).sort((a, b) => a.name.toUpperCase() > b.name.toUpperCase());
      let files = result.filter(a => !a.dir).sort((a, b) => a.name.toUpperCase() > b.name.toUpperCase());
      //Concaténation des deux tableaux
      result = folder.concat(files);
      //Si il y a eut une erreur, ou un dossier vide, on affiche un bouton de retour vers la racine du disque
      if (result[0] != null) {
        affichage(result);
      } else {
        createReturnButton("javascript:getFolderFiles('/')")
      }
    }
  }
  xhr.addEventListener("readystatechange", New, false);
  xhr.send(null);
}

function affichage(result) {

  if (result[0].parent.length != 0) {
    createReturnButton("javascript:getFolderFiles('" + result[0].parent + "')");
  }

  for (let i = 0; i < result.length; i++) {
    let a = document.createElement('a');
    let div = document.createElement('div');
    let img = document.createElement("img");
    a.classList.add("card");
    div.innerText = result[i].name;
    if (result[i].dir) {
      a.href = "javascript:getFolderFiles('" + result[i].fullpath + "')";
      img.src = "https://img.icons8.com/cotton/64/000000/folder-invoices--v1.png";
    } else {
      img.src = "https://img.icons8.com/cotton/64/000000/file.png"
    }

    a.append(img);
    a.append(div)
    contentD.append(a);
  }
}

function createReturnButton(fullpath) {
  let a = document.createElement('a');
  let div = document.createElement('div');
  let img = document.createElement("img");

  a.classList.add("card");
  a.href = fullpath;
  img.src = "https://img.icons8.com/carbon-copy/100/000000/back.png";

  a.append(img);
  a.append(div)
  returnD.append(a);
}
